package com.example.seminarproject.ui.rx

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.example.seminarproject.databinding.FragmentMergeBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MergeFragment : Fragment() {

    private lateinit var binding: FragmentMergeBinding
    lateinit var mergeViewModel: MergeViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        mergeViewModel = ViewModelProvider(this).get(MergeViewModel::class.java)
        binding = FragmentMergeBinding.inflate(inflater,container,false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        mergeViewModel.updateValue()

        binding.mergeBtn.setOnClickListener {

            // ViewModel 의 LiveData 에 접근
            mergeViewModel.currentValue.observe(viewLifecycleOwner, Observer {
                binding.mergeText.text = it.toString()
            })

        }

    }

}
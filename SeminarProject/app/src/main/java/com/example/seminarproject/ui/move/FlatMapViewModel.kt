package com.example.seminarproject.ui.move

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import io.reactivex.Observable
import io.reactivex.disposables.Disposable
import java.util.Date.from
import java.util.concurrent.TimeUnit
import javax.inject.Inject

@HiltViewModel
class FlatMapViewModel @Inject constructor() : ViewModel(){
}
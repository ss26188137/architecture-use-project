package com.example.seminarproject.ui.rx

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.example.seminarproject.databinding.FragmentFlatMap1Binding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class FlatMap1Fragment : Fragment() {

    private lateinit var binding: FragmentFlatMap1Binding
    lateinit var flatMap1ViewModel: FlatMap1ViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        flatMap1ViewModel = ViewModelProvider(this).get(FlatMap1ViewModel::class.java)
        binding = FragmentFlatMap1Binding.inflate(inflater,container,false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        flatMap1ViewModel.updateValue()

        binding.flatMap1Btn.setOnClickListener {

            // ViewModel 의 LiveData 에 접근
            flatMap1ViewModel.currentValue.observe(viewLifecycleOwner, Observer {
                binding.flatMap1Text.text = it.toString()
            })

        }

        // 함수 내부에서 emitter 가 직접 onNext, onComplete 등으로 데이터를 전달하는 연산자
    }

}
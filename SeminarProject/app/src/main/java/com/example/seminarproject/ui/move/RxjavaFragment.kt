package com.example.seminarproject.ui.move

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.NavController
import androidx.navigation.Navigation
import com.example.seminarproject.R
import com.example.seminarproject.databinding.FragmentRxjavaBinding
import com.example.seminarproject.databinding.FragmentStartBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class RxjavaFragment : Fragment() {

    lateinit var navController: NavController
    private lateinit var binding: FragmentRxjavaBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentRxjavaBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        navController = Navigation.findNavController(view)

        binding.goCreate.setOnClickListener {
            navController.navigate(R.id.action_rxjavaFragment2_to_createFragment)
        }

        binding.goFlatMap.setOnClickListener {
            navController.navigate(R.id.action_rxjavaFragment2_to_flatMapFragment)
        }

        binding.goFlowable.setOnClickListener {
            navController.navigate(R.id.action_rxjavaFragment2_to_flowableFragment)
        }

        binding.goMerge.setOnClickListener {
            navController.navigate(R.id.action_rxjavaFragment2_to_mergeFragment)
        }

        binding.goJust.setOnClickListener {
            navController.navigate(R.id.action_rxjavaFragment2_to_justFragment)
        }

        binding.goMap.setOnClickListener {
            navController.navigate(R.id.action_rxjavaFragment2_to_mapFragment)
        }

        binding.goFrom.setOnClickListener {
            navController.navigate(R.id.action_rxjavaFragment2_to_fromFragment)
        }

    }

}
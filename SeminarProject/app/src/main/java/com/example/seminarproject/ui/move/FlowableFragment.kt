package com.example.seminarproject.ui.move

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.NavController
import androidx.navigation.Navigation
import com.example.seminarproject.R
import com.example.seminarproject.databinding.FragmentFlowableBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class FlowableFragment : Fragment() {

    lateinit var navController: NavController
    private lateinit var binding: FragmentFlowableBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentFlowableBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        navController = Navigation.findNavController(view)

        binding.flo1.setOnClickListener {
            navController.navigate(R.id.action_flowableFragment_to_flow1Fragment)
        }

        binding.flo2.setOnClickListener {
            navController.navigate(R.id.action_flowableFragment_to_flow2Fragment)
        }

        binding.flo3.setOnClickListener {
            navController.navigate(R.id.action_flowableFragment_to_flow3Fragment)
        }
    }

}
package com.example.seminarproject

import android.annotation.SuppressLint
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.room.Room
import androidx.room.migration.Migration
import androidx.sqlite.db.SupportSQLiteDatabase
import com.example.seminarproject.R
import com.example.seminarproject.databinding.FragmentRoomBinding
import com.example.seminarproject.room.RecyclerAdapter
import com.example.seminarproject.room.RoomHelper
import com.example.seminarproject.room.RoomMemo
import com.example.seminarproject.ui.mvvm.MvvmViewModel
import com.example.seminarproject.ui.retrofit.RetrofitViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class RoomFragment : Fragment() {

    private lateinit var binding: FragmentRoomBinding

    var helper: RoomHelper? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentRoomBinding.inflate(inflater,container,false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        roomBuild()
    }
    fun roomBuild(){
        helper = Room.databaseBuilder(requireContext(), RoomHelper::class.java, "room_memo")
            .addMigrations(MigrateDatabase.MIGRATE_1_2)
            // room 은 기본적으로 서브 스레드에서 동작하기 때문에 allowMainThreadQueries 를 선언하지 않으면 앱이 동작을 멈출 수 있음
            .allowMainThreadQueries()
            .build()

        val adapter = RecyclerAdapter()
        adapter.helper = helper

        // roomHelper를 사용하여 어댑터의 데이터 목록에 세팅
        adapter.listData.addAll(helper?.roomMemoDao()?.getAll()?: listOf())
        // recyclerView 위젯의 adapter 속성에 생성할 어댑터 연결
        binding.recyclerMemo.adapter = adapter
        // 리사이클러뷰를 화면에 보여주는 형태를 결정하는 레이아웃 메니저를 연결
        binding.recyclerMemo.layoutManager = LinearLayoutManager(requireContext())

        // 저장 버튼을 누를 시 이벤트
        binding.buttonSave.setOnClickListener {
            if (binding.editMemo.text.toString().isNotEmpty()) {
                val memo = RoomMemo(binding.editMemo.text.toString(), System.currentTimeMillis())
                helper?.roomMemoDao()?.insert(memo)
                adapter.listData.clear()
                adapter.listData.addAll(helper?.roomMemoDao()?.getAll() ?: listOf())

                // 데이터가 추가된 다음 리사이클러뷰에 반영해주기 위한 함수
                adapter.notifyDataSetChanged()
                binding.editMemo.setText("")
            }
        }
    }

    // room 변경사항 적용하기(마이그레이션 기능으로 업데이트 정보를 반영할 수 있음)
    object MigrateDatabase {
        val MIGRATE_1_2 = object : Migration(1, 2) {
            override fun migrate(database: SupportSQLiteDatabase) {
                val alter = "ALTER table room_memo add column new_title text"
                database.execSQL(alter)
            }
        }
    }


}
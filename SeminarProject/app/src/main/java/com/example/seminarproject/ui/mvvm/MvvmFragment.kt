package com.example.seminarproject.ui.mvvm

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.example.seminarproject.databinding.FragmentMvvmBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MvvmFragment : Fragment() {

    private lateinit var binding: FragmentMvvmBinding
    lateinit var mvvmViewModel: MvvmViewModel

    companion object{
        const val TAG : String = "로그"
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        mvvmViewModel = ViewModelProvider(this).get(MvvmViewModel::class.java)
        binding = FragmentMvvmBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        // ViewModel 의 LiveData 에 접근
        mvvmViewModel.currentValue.observe(viewLifecycleOwner, Observer {
            binding.numberTextview.text = it.toString()
        })

        binding.plusBtn.setOnClickListener {
            val userInput = binding.userinputEdittext.text.toString().trim()
            if (userInput != "") {
                mvvmViewModel.updateValue(actionType = MvvmViewModel.ActionType.PLUS, userInput.toInt())
            }
        }
        binding.minusBtn.setOnClickListener {
            val userInput = binding.userinputEdittext.text.toString().trim()
            if (userInput != "") {
                mvvmViewModel.updateValue(actionType = MvvmViewModel.ActionType.MINUS, userInput.toInt())
            }
        }
    }

}
package com.example.seminarproject.ui.move

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavController
import androidx.navigation.Navigation
import com.example.seminarproject.R
import com.example.seminarproject.databinding.FragmentFlatMapBinding
import com.example.seminarproject.databinding.FragmentFlowableBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class FlatMapFragment : Fragment() {

    lateinit var navController: NavController
    private lateinit var binding: FragmentFlatMapBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentFlatMapBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        navController = Navigation.findNavController(view)

        binding.goflatMap1.setOnClickListener {
            navController.navigate(R.id.action_flatMapFragment_to_flatMap1Fragment)
        }

        binding.goEmptyFlatMap.setOnClickListener {
            navController.navigate(R.id.action_flatMapFragment_to_emptyFlatMapFragment)
        }

        binding.goErrorFlatMap.setOnClickListener {
            navController.navigate(R.id.action_flatMapFragment_to_errorFlatMapFragment)
        }
    }

}
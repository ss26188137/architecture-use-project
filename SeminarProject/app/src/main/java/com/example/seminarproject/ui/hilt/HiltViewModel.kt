package com.example.seminarproject.ui.hilt

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class HiltViewModel @Inject constructor() : ViewModel() {

    val _currentValue = MutableLiveData<String>()

    val currentValue : LiveData<String>
        get() = _currentValue

    init {
        _currentValue.value = ""
    }

    fun updateValue(input: String){
        _currentValue.value = input
    }
}
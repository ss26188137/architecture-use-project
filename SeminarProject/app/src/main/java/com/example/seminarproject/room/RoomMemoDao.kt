package com.example.seminarproject.room

import androidx.room.*

// DB 의 DML(SELECT, INSERT, UPDATE, DELETE) 메서드 구현을 위해 인터페이스 생성
@Dao
interface RoomMemoDao {
    @Query("select * from room_memo")
    fun getAll(): List<RoomMemo>    // 해당 값을 List 형태로 불러옴

    // onConflict 를 적용하면 동일한 값이 입력됐을 때 update 쿼리를 실행시켜 줌
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(memo: RoomMemo)

    @Delete
    fun delete(memo: RoomMemo)
}
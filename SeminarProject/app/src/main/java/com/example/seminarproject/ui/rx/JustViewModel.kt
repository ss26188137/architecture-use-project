package com.example.seminarproject.ui.rx

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import io.reactivex.Observable
import io.reactivex.disposables.Disposable
import javax.inject.Inject

@HiltViewModel
class JustViewModel @Inject constructor() : ViewModel(){

    private val _currentValue = MutableLiveData<String>()

    // 변경되지 않는 데이터를 가져올 때 이름을 _ 없이 설정
    // 공개적으로 가져오는 변수는 private 아닌 public 으로 외부에서도 접근 가능하도록 설정
    // 하지만 값을 직접 LiveData 에 접근하지 않고 ViewModel 을 통해 가져올 수 있도록 설정
    val currentValue : LiveData<String>
        get() = _currentValue


    // 초기값 설정
    init {
        Log.d(CreateViewModel.TAG,"JustViewModel - 값 호출")
        _currentValue.value = ""
    }

    // ViewModel 이 가지고 있는 값을 변경하는 메소드
    fun updateValue(){

        Observable.just(1, 2, 3, 4)
            .subscribe(
                { _currentValue.value = _currentValue.value.toString() + "onNext: " + it }, // onNext: Consumer
                { _currentValue.value = _currentValue.value.toString() + "\nonError: " }, // onError: Consumer
                { _currentValue.value = _currentValue.value.toString() + "\nonComplete: " }, // onComplete: Consumer
                { _currentValue.value = _currentValue.value.toString() + "\nonSubscribe: " } // onSubscribe: Consumer
            )
            .dispose()
    }
}

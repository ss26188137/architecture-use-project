package com.example.seminarproject.model

data class SeoulPublicLibraryInfo(
    val RESULT: RESULT?,
    val list_total_count: Int?,
    val row: List<Row>?
)
package com.example.seminarproject.model

data class RESULT(
    val CODE: String?,
    val MESSAGE: String?
)
package com.example.seminarproject.ui.rx

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.example.seminarproject.databinding.FragmentFromBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class FromFragment : Fragment() {

    private lateinit var binding: FragmentFromBinding
    lateinit var fromViewModel: FromViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        fromViewModel = ViewModelProvider(this).get(FromViewModel::class.java)
        binding = FragmentFromBinding.inflate(inflater,container,false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        fromViewModel.updateValue()

        binding.fromBtn.setOnClickListener {

            // ViewModel 의 LiveData 에 접근
            fromViewModel.currentValue.observe(viewLifecycleOwner, Observer {
                binding.fromText.text = it.toString()
            })

        }

        // 함수 내부에서 emitter 가 직접 onNext, onComplete 등으로 데이터를 전달하는 연산자
    }

}

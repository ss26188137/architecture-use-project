package com.example.seminarproject.ui.rx

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import io.reactivex.Flowable
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import java.util.concurrent.TimeUnit
import javax.inject.Inject

@HiltViewModel
class Flow2ViewModel @Inject constructor() : ViewModel() {


    private val _currentValue = MutableLiveData<String>()

    // 변경되지 않는 데이터를 가져올 때 이름을 _ 없이 설정
    // 공개적으로 가져오는 변수는 private 아닌 public 으로 외부에서도 접근 가능하도록 설정
    // 하지만 값을 직접 LiveData 에 접근하지 않고 ViewModel 을 통해 가져올 수 있도록 설정
    val currentValue : LiveData<String>
        get() = _currentValue


    // 초기값 설정
    init {
        Log.d(CreateViewModel.TAG,"Flo1ViewModel - 값 호출")
        _currentValue.value = ""
    }

    // ViewModel 이 가지고 있는 값을 변경하는 메소드
    fun updateValue(){

        val disposable : Disposable = Flowable.interval(10, TimeUnit.MILLISECONDS)
            .observeOn(Schedulers.io())
            .map { item ->
                Thread.sleep(2000)
                item.also {
                    println("아이템 발행: $item")
                }
            }
            .subscribe({ item ->
                Thread.sleep(2000)
                println("아이템 소비 : $item")
            }) { throwable ->
                throwable.printStackTrace()
            }
        Thread.sleep(30 * 1000L)
        disposable.dispose()
    }

}
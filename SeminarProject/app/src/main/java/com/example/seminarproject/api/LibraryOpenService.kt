package com.example.seminarproject.api

import com.example.seminarproject.model.Library
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path

interface LibraryOpenService {

    @GET("{api_key}/json/SeoulPublicLibraryInfo/1/200")
    fun getLibrary(@Path("api_key") key : String) : Call<Library>

}
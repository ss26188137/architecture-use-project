package com.example.seminarproject.ui.rx

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import io.reactivex.Observable
import javax.inject.Inject

@HiltViewModel
class ErrorFlatMapViewModel @Inject constructor() : ViewModel(){

    private val _currentValue = MutableLiveData<String>()

    // 변경되지 않는 데이터를 가져올 때 이름을 _ 없이 설정
    // 공개적으로 가져오는 변수는 private 아닌 public 으로 외부에서도 접근 가능하도록 설정
    // 하지만 값을 직접 LiveData 에 접근하지 않고 ViewModel 을 통해 가져올 수 있도록 설정
    val currentValue : LiveData<String>
        get() = _currentValue


    // 초기값 설정
    init {
        Log.d(CreateViewModel.TAG,"ErrorFlatMapViewModel - 값 호출")
        _currentValue.value = ""
    }

    // ViewModel 이 가지고 있는 값을 변경하는 메소드
    fun updateValue(){

        Observable.just(1, 2, 0, 4, 5)
            .map { 10 / it }
            .flatMap({
                Observable.just(it) // 일반 데이터 통지
            }, {
                Observable.just(-1) // 에러 발생시 데이터 통지
            }, {
                Observable.just(100) // 완료시 데이터 100 통지
            })
            .subscribe{_currentValue.value = _currentValue.value.toString() + "\n" + it}
            .dispose()

        // 원본 Observable 이 0에서 에러를 통지해 완료를 통지하지 못했기 때문에 onCompleteSupplier의 함수형 인터페이스는 실행되지 않음
    }

}
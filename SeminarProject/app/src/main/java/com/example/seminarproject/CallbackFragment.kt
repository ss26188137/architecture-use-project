package com.example.seminarproject

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import com.example.seminarproject.databinding.FragmentCallbackBinding
import com.example.seminarproject.ui.mvvm.MvvmViewModel
import dagger.hilt.android.AndroidEntryPoint
import io.reactivex.Observable
import io.reactivex.schedulers.Schedulers
import okhttp3.*
import java.io.IOException

@AndroidEntryPoint
class CallbackFragment : Fragment() {

    private lateinit var binding: FragmentCallbackBinding
    lateinit var callbackViewModel: CallbackViewModel

    val FIRST_URL = "https://api.github.com/zen"
    val SECOND_URL = "https://raw.githubusercontent.com/yudong80/reactivejava/master/samples/callback_hell"
    val client = OkHttpClient()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        callbackViewModel = ViewModelProvider(this).get(CallbackViewModel::class.java)
        binding = FragmentCallbackBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        callbackViewModel.updateValue()
        callBackHell()
        callBackHeaven()

    }

    fun callBackHell() {
        val client = OkHttpClient()
        val request = Request.Builder()
            .url(FIRST_URL)
            .build()

        // HTTP GET 명령으로 성공하면 가져온 내용을 출력하고 실패하면 메서드 호출 스택을 출력
        client.newCall(request).enqueue(object : Callback {
            override fun onFailure(call: Call, e: IOException) {
                e.printStackTrace()
            }

            @Throws(IOException::class)
            override fun onResponse(call: Call, response: Response) {
                Log.d("Tag", "callBackHell :" + response.toString())
                activity!!.runOnUiThread { binding.result.text = binding.result.text.toString() + "\ncallBackHell :" + response.toString() }

                // 실행 결과를 얻은 후 두 번째 URL을 호출할 때는 지역 변수를 사용할 수 없으므로 객체의 멤버 변수로 선언해야 함.
                // 즉, 첫 번째 호출의 성공과 실패가 있고 그것을 기준으로 두 번째 호출의 성공과 실패가 있기 때문에 이를 모두 고려해 코드를 작성해야 함.
                val request = Request.Builder()
                    .url(SECOND_URL)
                    .build()
                client.newCall(request).enqueue(object : Callback {
                    override fun onFailure(call: Call, e: IOException) {
                        e.printStackTrace()
                    }

                    @Throws(IOException::class)
                    override fun onResponse(call: Call, response: Response) {
                        Log.d("Tag", "callBackHell :" + response.toString())
                        activity!!.runOnUiThread { binding.result.text = binding.result.text.toString() + "\ncallBackHell :" + response.toString() }
                    }
                })
            }
        })
    }

    // 동시성, 가독성으로 훨씬 코드가 보기 쉬워짐.
    fun callBackHeaven() {
        val source = Observable.just(FIRST_URL)
            .subscribeOn(Schedulers.io())
            .map { it ->
                get(it)
            }
            .concatWith(
                Observable.just(SECOND_URL)
                    .map { it -> get(it) })
        source.subscribe { it ->
            Log.d("Tag", "callBackHeaven :" + it.toString())
            requireActivity().runOnUiThread {binding.result.text = binding.result.text.toString() + "\ncallBackHeaven :" + it.toString() }
        }
    }

    @Throws(IOException::class)
    fun get(url: String): String {
        val request = Request.Builder()
            .url(url)
            .build()
        try {
            val res = client.newCall(request).execute()
            return res.toString()
        } catch (e: IOException) {
            println(e.message)
            throw e
        }
    }
}
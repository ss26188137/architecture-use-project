package com.example.seminarproject.room

import androidx.room.Database
import androidx.room.RoomDatabase

// entities 는 Room 라이브러리가 사용한 엔티티 클래스 목록
// version 은 데이터베이스의 버전을 나타냄
// exportSchema 가 true 면 스키마 정보를 파일로 출력
@Database(entities = arrayOf(RoomMemo::class), version = 1, exportSchema = false)
abstract class RoomHelper : RoomDatabase() {

    // 실제로 사용될 구현체 메소드
    abstract  fun roomMemoDao(): RoomMemoDao
}
package com.example.seminarproject.ui.move

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.NavController
import androidx.navigation.Navigation
import com.example.seminarproject.R
import com.example.seminarproject.databinding.FragmentStartBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class StartFragment : Fragment() {
    lateinit var navController: NavController
    private lateinit var binding: FragmentStartBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentStartBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        navController = Navigation.findNavController(view)

        binding.goHiltBtn.setOnClickListener {
            navController.navigate(R.id.action_mainFragment_to_hiltFragment2)
        }

        binding.goMvvmBtn.setOnClickListener {
            navController.navigate(R.id.action_mainFragment_to_mvvmFragment2)
        }

        binding.goRetroBtn.setOnClickListener {
            navController.navigate(R.id.action_mainFragment_to_retrofitFragment2)
        }

        binding.goRoomBtn.setOnClickListener {
            navController.navigate(R.id.action_mainFragment_to_roomFragment2)
        }

        binding.gpRxBtn.setOnClickListener {
            navController.navigate(R.id.action_mainFragment_to_rxjavaFragment2)
        }

        binding.goCallbackBtn.setOnClickListener {
            navController.navigate(R.id.action_mainFragment_to_callbackFragment)
        }

    }
}
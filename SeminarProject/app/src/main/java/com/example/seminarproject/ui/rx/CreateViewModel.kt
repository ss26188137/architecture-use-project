package com.example.seminarproject.ui.rx

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import io.reactivex.Observable
import io.reactivex.disposables.Disposable
import javax.inject.Inject


@HiltViewModel
class CreateViewModel @Inject constructor() : ViewModel(){


    companion object{
        const val TAG : String = "로그"
    }

    // MutableLiveData 는 수정 가능하며, LivaData는 값 변동이 안되는 읽기 전용

    // 내부에서 설정하는 자료형을 Mutable로 하여 변경가능하도록 설정
    private val _currentValue = MutableLiveData<String>()

    // 변경되지 않는 데이터를 가져올 때 이름을 _ 없이 설정
    // 공개적으로 가져오는 변수는 private 아닌 public 으로 외부에서도 접근 가능하도록 설정
    // 하지만 값을 직접 LiveData 에 접근하지 않고 ViewModel 을 통해 가져올 수 있도록 설정
    val currentValue : LiveData<String>
        get() = _currentValue


    // 초기값 설정
    init {
        Log.d(TAG,"CreateViewModel - 값 호출")
        _currentValue.value = ""
    }

    // ViewModel 이 가지고 있는 값을 변경하는 메소드
    fun updateValue(){

        Observable.create<String> { emitter ->
            emitter.onNext("Hello")
            emitter.onNext("RxJava")
            emitter.onComplete()
        }
            .subscribe { _currentValue.value = _currentValue.value.toString() + "\n" + it.toString() }
            .dispose()


            //_currentValue.value = input
    }
}
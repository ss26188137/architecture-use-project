package com.example.seminarproject.ui.rx

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.example.seminarproject.databinding.FragmentFlow3Binding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class Flow3Fragment : Fragment() {

    private lateinit var binding: FragmentFlow3Binding
    lateinit var flow3ViewModel: Flow3ViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        flow3ViewModel = ViewModelProvider(this).get(Flow3ViewModel::class.java)
        binding = FragmentFlow3Binding.inflate(inflater,container,false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        flow3ViewModel.updateValue()

        binding.flo3Btn.setOnClickListener {

            // ViewModel 의 LiveData 에 접근
            flow3ViewModel.currentValue.observe(viewLifecycleOwner, Observer {
                binding.flo3Text.text = it.toString()
            })

        }

    }
}
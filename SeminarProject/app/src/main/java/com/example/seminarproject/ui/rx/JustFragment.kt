package com.example.seminarproject.ui.rx

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.example.seminarproject.databinding.FragmentJustBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class JustFragment : Fragment() {

    private lateinit var binding: FragmentJustBinding
    lateinit var justViewModel: JustViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        justViewModel = ViewModelProvider(this).get(JustViewModel::class.java)
        binding = FragmentJustBinding.inflate(inflater,container,false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        justViewModel.updateValue()

        binding.justBtn.setOnClickListener {

            // ViewModel 의 LiveData 에 접근
            justViewModel.currentValue.observe(viewLifecycleOwner, Observer {
                binding.justText.text = it.toString()
            })

        }

    }

}
